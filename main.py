from time import sleep
import xml.etree.ElementTree

print('Vixen Parser')
config = xml.etree.ElementTree.parse('test_Network.xml').getroot() # Parse vixen configuration file
resolution = int(config[0].text) / 1000 # Get resolution and convert to seconds
csv_file = config[1].text # Get CSV file from config

file = open(csv_file, 'r')

for vixen_show in file: # Iterate through each line

    pin_array = vixen_show.split(',')
    pin_num = 0
    on_channels = ""

    for value in pin_array:
        pin_num += 1
        if (int(value) != 0): # Check if channel is on
            on_channels += str(pin_num) + ", "
    if (on_channels == ""):
        print("Channels are off")
    else:
        print(on_channels) # Print all channels that are on - could replace this with GPIO commands
    sleep(resolution) # Sleep for the set resolution
else:
    print("DONE")
    exit()
